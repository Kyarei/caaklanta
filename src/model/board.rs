//! Items related to the game board.

use std::fmt::Display;

use crate::model::tile::TileId;

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Space {
    /// A regular space.
    XX,
    /// A double-letter space.
    L2,
    /// A triple-letter space.
    L3,
    /// A double-word space.
    W2,
    /// A triple-word space.
    W3,
}

pub const BOARD_SIZE: usize = 15;
// The board is symmetric across both axes, so we only need to encode
// the upper left quadrant.
static BOARD: [[Space; (BOARD_SIZE + 1) / 2]; (BOARD_SIZE + 1) / 2] = {
    use Space::*;
    [
        [W3, XX, XX, L2, XX, XX, XX, W3],
        [XX, W2, XX, XX, XX, L3, XX, XX],
        [XX, XX, W2, XX, XX, XX, L2, XX],
        [L2, XX, XX, W2, XX, XX, XX, L2],
        [XX, XX, XX, XX, W2, XX, XX, XX],
        [XX, L3, XX, XX, XX, L3, XX, XX],
        [XX, XX, L2, XX, XX, XX, L2, XX],
        [W3, XX, XX, L2, XX, XX, XX, W2],
    ]
};

#[inline]
pub fn get_board_space(row: usize, col: usize) -> Space {
    BOARD[row.min(BOARD_SIZE - 1 - row)][col.min(BOARD_SIZE - 1 - col)]
}

pub fn parse_col(c: char) -> Option<usize> {
    match c {
        't' => Some(0),
        'k' => Some(1),
        'x' => Some(2),
        's' => Some(3),
        'n' => Some(4),
        'v' => Some(5),
        'f' => Some(6),
        'm' => Some(7),
        'd' => Some(8),
        'g' => Some(9),
        'p' => Some(10),
        'b' => Some(11),
        'h' => Some(12),
        'y' => Some(13),
        'c' => Some(14),
        _ => None,
    }
}

pub fn get_col_char(col: usize) -> char {
    b"tkxsnvfmdgpbhyc"[col] as char
}

/// A tile played on the board.
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum BoardTile {
    Letter(TileId),
    Blank(TileId),
}

impl BoardTile {
    pub fn required_tile(self) -> TileId {
        match self {
            BoardTile::Letter(letter) => letter,
            BoardTile::Blank(_) => TileId::BLANK,
        }
    }

    pub fn counted_tile(self) -> TileId {
        match self {
            BoardTile::Letter(letter) => letter,
            BoardTile::Blank(letter) => letter,
        }
    }
}

impl Display for BoardTile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            BoardTile::Letter(t) => Display::fmt(t, f),
            BoardTile::Blank(t) => write!(f, "[{t}]"),
        }
    }
}

/// The state of a game board with tiles played.
#[derive(Clone, Debug)]
pub struct Board {
    pub tiles: [[Option<BoardTile>; BOARD_SIZE]; BOARD_SIZE],
}

impl Board {
    pub fn new() -> Self {
        Self {
            tiles: [[None; BOARD_SIZE]; BOARD_SIZE],
        }
    }

    pub fn get(&self, row: usize, col: usize) -> Option<Option<BoardTile>> {
        Some(*self.tiles.get(row)?.get(col)?)
    }

    pub fn put(&mut self, row: usize, col: usize, tile: Option<BoardTile>) -> Option<()> {
        *self.tiles.get_mut(row)?.get_mut(col)? = tile;
        Some(())
    }

    pub fn has_occupied_neighbors(&self, row: usize, col: usize) -> bool {
        // Some(Some(_)) => in bounds and occupied by a tile
        matches!(self.get(row.wrapping_sub(1), col), Some(Some(_)))
            || matches!(self.get(row.wrapping_add(1), col), Some(Some(_)))
            || matches!(self.get(row, col.wrapping_sub(1)), Some(Some(_)))
            || matches!(self.get(row, col.wrapping_add(1)), Some(Some(_)))
    }
}

impl Default for Board {
    fn default() -> Self {
        Self::new()
    }
}

/// The orientation of a play.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Orientation {
    Right,
    Down,
}

impl Orientation {
    /// Gets the orientation perpendicular to this one.
    pub fn perp(self) -> Self {
        match self {
            Orientation::Right => Orientation::Down,
            Orientation::Down => Orientation::Right,
        }
    }
}

/// The starting point and direction of a play.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Origin {
    pub row: usize,
    pub col: usize,
    pub orientation: Orientation,
}

impl Origin {
    pub fn get(&self, index: usize) -> (usize, usize) {
        match self.orientation {
            Orientation::Right => (self.row, self.col + index),
            Orientation::Down => (self.row + index, self.col),
        }
    }

    pub fn unget(&self, row: usize, col: usize) -> Option<usize> {
        match self.orientation {
            Orientation::Right => (row == self.row)
                .then_some(col.checked_sub(self.col))
                .flatten(),
            Orientation::Down => (col == self.col)
                .then_some(row.checked_sub(self.row))
                .flatten(),
        }
    }
}

impl Display for Origin {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.orientation {
            Orientation::Right => write!(f, "{}{}", self.row + 1, get_col_char(self.col)),
            Orientation::Down => write!(f, "{}{}", get_col_char(self.col), self.row + 1),
        }
    }
}

/// A range of tiles that contains a word.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Extent {
    pub origin: Origin,
    pub num_tiles: usize,
}

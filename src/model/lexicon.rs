use std::{
    collections::HashSet,
    fmt::Debug,
    io::{self, BufRead},
    mem,
};

use smallvec::SmallVec;

use super::tile::TileId;

pub struct Lexicon {
    words: HashSet<SmallVec<[TileId; 8]>>,
}

impl Lexicon {
    pub fn read(file: &mut impl BufRead) -> io::Result<Self> {
        let mut words: HashSet<SmallVec<[TileId; 8]>> = Default::default();
        let mut line_buf = String::new();
        let mut pending_word: SmallVec<[TileId; 8]> = Default::default();
        while file.read_line(&mut line_buf)? > 0 {
            let line = line_buf.trim();
            if line.contains('q') {
                line_buf.clear();
                continue;
            }
            for c in line.chars() {
                let tile_id = TileId::from_char(c).ok_or_else(|| {
                    io::Error::new(io::ErrorKind::InvalidData, format!("invalid char {c}"))
                })?;
                pending_word.push(tile_id);
            }
            if pending_word.len() < 2 {
                line_buf.clear();
                pending_word.clear();
                continue;
            }
            words.insert(mem::take(&mut pending_word));
            line_buf.clear();
        }
        Ok(Self { words })
    }

    pub fn contains(&self, word: &[TileId]) -> bool {
        self.words.contains(word)
    }
}

impl Debug for Lexicon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Lexicon ({} words)", self.words.len())
    }
}

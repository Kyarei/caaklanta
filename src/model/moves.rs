//! Items related to game moves.

use std::fmt::Display;

use arrayvec::ArrayVec;
use smallvec::SmallVec;

use crate::model::board::{get_board_space, Space};

use super::{
    board::{Board, BoardTile, Extent, Orientation, Origin},
    game::{Game, RACK_SIZE},
    lexicon::Lexicon,
    tile::TileId,
};

/// A move in a game.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Move {
    /// Play a word on the board.
    Play(PlayMove),
    /// Skip a turn.
    Pass,
    /// Swap one or more letters.
    Swap(SwapMove),
}

impl Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Move::Play(m) => Display::fmt(m, f),
            Move::Pass => f.write_str("sef"),
            Move::Swap(m) => Display::fmt(m, f),
        }
    }
}

/// A move of a word played on the board.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PlayMove {
    pub origin: Origin,
    pub word: SmallVec<[BoardTile; 8]>,
}

impl PlayMove {
    pub fn get(&self, row: usize, col: usize) -> Option<BoardTile> {
        let i = self.origin.unget(row, col)?;
        self.word.get(i).copied()
    }

    pub fn extent(&self) -> Extent {
        Extent {
            origin: self.origin,
            num_tiles: self.word.len(),
        }
    }
}

impl Display for PlayMove {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.origin, f)?;
        f.write_str(" ")?;
        for tile in &self.word {
            Display::fmt(tile, f)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SwapMove {
    pub letters: ArrayVec<TileId, RACK_SIZE>,
}

impl Display for SwapMove {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("-")?;
        for tile in &self.letters {
            Display::fmt(tile, f)?;
        }
        Ok(())
    }
}

/// Information about a successful move.
#[derive(Debug, Clone)]
pub struct MoveFeedback {
    /// The number of points awarded by this move.
    pub score_added: i32,
}

/// Information about an unsuccessful move.
#[derive(Debug, Clone)]
pub enum MoveError {
    /// The player doesn’t have the tiles needed for this move.
    InvalidTiles,
    /// The word could not be played in this position.
    InvalidPosition,
    /// This move doesn’t play any tiles.
    NoTilesPlayed,
    /// This move doesn’t play any words.
    NoWordsPlayed,
    /// One or more invalid words were played.
    InvalidWords(Vec<String>),
    /// The bag has too few tiles to swap.
    EmptyBag,
}

impl Display for MoveError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidTiles => f.write_str("ti si kots len tu solet"),
            Self::InvalidPosition => f.write_str("xaka dim"),
            Self::NoTilesPlayed => f.write_str("tu solet ev yuu kots"),
            Self::NoWordsPlayed => f.write_str("tu solet ev yuu vet"),
            Self::InvalidWords(words) => {
                f.write_str("vet dim: ")?;
                let mut first = true;
                for word in words {
                    if !first {
                        f.write_str(", ")?;
                    }
                    first = false;
                    f.write_str(word)?;
                }
                Ok(())
            }
            Self::EmptyBag => f.write_str("el fut vil kots im mesl til kots kalt 7"),
        }
    }
}

impl Move {
    /// Applies this move to the game state.
    ///
    /// If this fails, then neither the board nor the player is
    /// affected.
    pub fn apply(&self, game: &mut Game, lexicon: &Lexicon) -> Result<MoveFeedback, MoveError> {
        let result = match self {
            Move::Play(m) => m.apply(game, lexicon),
            Move::Pass => Ok(MoveFeedback { score_added: 0 }),
            Move::Swap(m) => m.apply(game),
        }?;
        game.players[game.current_player].replenish_from_bag(&mut game.bag, &mut game.rng);
        game.current_player = (game.current_player + 1) % game.players.len();
        if result.score_added == 0 {
            game.consecutive_scoreless_moves += 1;
        } else {
            game.consecutive_scoreless_moves = 0;
        }
        Ok(result)
    }

    pub fn canonicalize(&mut self, board: &Board) {
        match self {
            Move::Play(m) => {
                let overlaid = OverlaidBoard { play: m, board };
                let ext = m.extent();
                let mut ext = overlaid.extend(&ext);
                let ext = if ext.num_tiles < 2 {
                    ext.origin.orientation = ext.origin.orientation.perp();
                    overlaid.extend(&ext)
                } else {
                    ext
                };
                let origin = ext.origin;
                let tiles = (0..ext.num_tiles)
                    .map(|i| {
                        let (row, col) = origin.get(i);
                        overlaid.get(row, col).unwrap().unwrap()
                    })
                    .collect();
                m.origin = origin;
                m.word = tiles;
            }
            Move::Pass => (),
            Move::Swap(m) => {
                m.letters.sort_unstable();
                if m.letters.is_empty() {
                    *self = Move::Pass;
                }
            }
        }
    }
}

impl PlayMove {
    pub fn apply(&self, game: &mut Game, lexicon: &Lexicon) -> Result<MoveFeedback, MoveError> {
        let player = &game.players[game.current_player];
        let board = &game.board;

        // Which tiles have been played?
        let mut deduction_mask = ArrayVec::from([false; RACK_SIZE]);
        deduction_mask.truncate(player.rack.len());

        // Make sure we have the tiles needed to play the word in question.
        let mut meets_adjacency_reqs = false;
        for (i, letter) in self.word.iter().enumerate() {
            let (row, col) = self.origin.get(i);
            let existing_tile = board.get(row, col).ok_or(MoveError::InvalidPosition)?;
            match existing_tile {
                None => {
                    // Check that we have the tile needed to play this.
                    // We require blank tiles to be explicitly played with brackets.
                    // We could relax this requirement, but we’d have to make sure to place a blank tile when needed.
                    if let Some((_tile, deduct)) =
                        player.rack.iter().zip(deduction_mask.iter_mut()).find(
                            |(tile, deducted)| !**deducted && **tile == letter.required_tile(),
                        )
                    {
                        // Strike this off so it can’t be used again.
                        *deduct = true;
                    } else {
                        // We don’t have the tiles needed for this.
                        return Err(MoveError::InvalidTiles);
                    }
                    if !meets_adjacency_reqs {
                        let on_start_square = row == 7 && col == 7;
                        meets_adjacency_reqs =
                            on_start_square || board.has_occupied_neighbors(row, col);
                    }
                }
                Some(existing_tile) => {
                    // Check that the existing tile doesn’t clash with what we want to play.
                    if existing_tile != *letter {
                        return Err(MoveError::InvalidPosition);
                    }
                }
            }
        }
        // Check that we’ve played at least one tile…
        if !deduction_mask.iter().any(|d| *d) {
            return Err(MoveError::NoTilesPlayed);
        }
        // and that we’ve played next to at least one existing tile.
        if !meets_adjacency_reqs {
            return Err(MoveError::InvalidPosition);
        }

        let oboard = OverlaidBoard { play: self, board };
        let mut score = oboard.evaluate_played_move(lexicon)?;

        // Move was successful – commit the results.
        let player = &mut game.players[game.current_player];
        let board = &mut game.board;
        for (i, letter) in self.word.iter().enumerate() {
            let (row, col) = self.origin.get(i);
            board.put(row, col, Some(*letter));
        }
        {
            let mut i = 0;
            let old_len = player.rack.len();
            player.rack.retain(|_elem| {
                let deducted = deduction_mask[i];
                i += 1;
                !deducted
            });
            if old_len >= RACK_SIZE && player.rack.is_empty() {
                score += 50; // Award bonus
            }
        }
        player.score += score;

        Ok(MoveFeedback { score_added: score })
    }
}

/// Combines a board with a pending play.
///
/// This is used so that moves can be checked for validity before
/// being finalized.
#[derive(Debug)]
struct OverlaidBoard<'a> {
    play: &'a PlayMove,
    board: &'a Board,
}

impl<'a> OverlaidBoard<'a> {
    pub fn get(&self, row: usize, col: usize) -> Option<Option<BoardTile>> {
        if let Some(tile) = self.play.get(row, col) {
            return Some(Some(tile));
        }
        self.board.get(row, col)
    }

    pub fn evaluate_played_move(&self, lexicon: &Lexicon) -> Result<i32, MoveError> {
        let mut illegal_words = Vec::new();
        let mut score = 0;
        let mut played = false;
        let through_ext = self.extend(&self.play.extent());
        score += self.evaluate_extent(&through_ext, lexicon, &mut illegal_words, &mut played);
        for i in 0..self.play.word.len() {
            let (row, col) = self.play.origin.get(i);
            if self.board.get(row, col).flatten().is_some() {
                continue;
            }
            let extent = Extent {
                origin: Origin {
                    row,
                    col,
                    orientation: self.play.origin.orientation.perp(),
                },
                num_tiles: 1,
            };
            let extent = self.extend(&extent);
            score += self.evaluate_extent(&extent, lexicon, &mut illegal_words, &mut played);
        }
        if !illegal_words.is_empty() {
            Err(MoveError::InvalidWords(illegal_words))
        } else if !played {
            Err(MoveError::NoWordsPlayed)
        } else {
            Ok(score)
        }
    }

    pub fn extend(&self, extent: &Extent) -> Extent {
        let mut startward = 0;
        let mut endward = 0;
        let row = extent.origin.row;
        let col = extent.origin.col;
        let n = extent.num_tiles;
        match extent.origin.orientation {
            Orientation::Right => {
                loop {
                    let tile = self.get(row, col.wrapping_sub(startward + 1));
                    if !matches!(tile, Some(Some(_))) {
                        break;
                    }
                    startward += 1;
                }
                loop {
                    let tile = self.get(row, col.wrapping_add(endward + n));
                    if !matches!(tile, Some(Some(_))) {
                        break;
                    }
                    endward += 1;
                }
                Extent {
                    origin: Origin {
                        row,
                        col: col - startward,
                        orientation: Orientation::Right,
                    },
                    num_tiles: extent.num_tiles + startward + endward,
                }
            }
            Orientation::Down => {
                loop {
                    let tile = self.get(row.wrapping_sub(startward + 1), col);
                    if !matches!(tile, Some(Some(_))) {
                        break;
                    }
                    startward += 1;
                }
                loop {
                    let tile = self.get(row.wrapping_add(endward + n), col);
                    if !matches!(tile, Some(Some(_))) {
                        break;
                    }
                    endward += 1;
                }
                Extent {
                    origin: Origin {
                        row: row - startward,
                        col,
                        orientation: Orientation::Down,
                    },
                    num_tiles: extent.num_tiles + startward + endward,
                }
            }
        }
    }

    pub fn evaluate_extent(
        &self,
        extent: &Extent,
        lexicon: &Lexicon,
        invalid_words_out: &mut Vec<String>,
        played: &mut bool,
    ) -> i32 {
        // Need at least 2 letters to count as a word
        if extent.num_tiles < 2 {
            return 0;
        }
        let mut word = SmallVec::<[TileId; 16]>::new();
        let mut score = 0;
        let mut multi = 1;
        for i in 0..extent.num_tiles {
            let (row, col) = extent.origin.get(i);
            let board_tile = self.get(row, col).unwrap().unwrap();
            let tile = board_tile.counted_tile();
            word.push(tile);
            let space = if matches!(self.board.get(row, col), Some(Some(_))) {
                Space::XX // Already used
            } else {
                get_board_space(row, col)
            };
            // Make sure the blank is counted for 0 points!
            let letter_score: i32 = board_tile.required_tile().get_data().value.into();
            score += letter_score
                * match space {
                    Space::L2 => 2,
                    Space::L3 => 3,
                    _ => 1,
                };
            multi *= match space {
                Space::W2 => 2,
                Space::W3 => 3,
                _ => 1,
            }
        }
        if !lexicon.contains(&word) {
            invalid_words_out.push(word.into_iter().map(|t| t.get_data().letter).collect());
        }
        *played = true;
        score * multi
    }
}

impl SwapMove {
    pub fn apply(&self, game: &mut Game) -> Result<MoveFeedback, MoveError> {
        if game.bag.len() < RACK_SIZE {
            return Err(MoveError::EmptyBag);
        }

        let player = &game.players[game.current_player];

        // Which tiles have been swapped?
        let mut deduction_mask = ArrayVec::from([false; RACK_SIZE]);
        deduction_mask.truncate(player.rack.len());

        for letter in &self.letters {
            // Check that we have the tile that we want to discard.
            if let Some((_tile, deduct)) = player
                .rack
                .iter()
                .zip(deduction_mask.iter_mut())
                .find(|(tile, deducted)| !**deducted && **tile == *letter)
            {
                // Strike this off so it can’t be used again.
                *deduct = true;
            } else {
                // We don’t have the tiles needed for this.
                return Err(MoveError::InvalidTiles);
            }
        }

        let player = &mut game.players[game.current_player];
        {
            let mut i = 0;
            player.rack.retain(|_elem| {
                let deducted = deduction_mask[i];
                i += 1;
                !deducted
            });
        }

        Ok(MoveFeedback { score_added: 0 })
    }
}

#[cfg(test)]
mod tests {
    use std::{fs::File, io::BufReader};

    use arrayvec::ArrayVec;
    use poise::serenity_prelude::UserId;
    use rand::{rngs::StdRng, SeedableRng};
    use smallvec::smallvec;

    use crate::model::{
        board::{Board, BoardTile, Orientation, Origin},
        game::{Game, Player},
        lexicon::Lexicon,
        tile::TileId,
    };

    use super::{Move, PlayMove};

    #[test]
    fn test_moves() {
        /*
              m
              e
             atax
           ARKa
              s
              t

            This should score:
            - 5 for ‘arka’ (r on the 2L)
            - 2 for ‘ak’
            - no points for ‘metast’ (already on the board)
        */
        let lexicon =
            Lexicon::read(&mut BufReader::new(File::open("data/veteda.txt").unwrap())).unwrap();
        let the_move = Move::Play(PlayMove {
            origin: Origin {
                row: 8,
                col: 5,
                orientation: Orientation::Right,
            },
            word: smallvec![
                BoardTile::Letter(TileId(20)),
                BoardTile::Letter(TileId(15)),
                BoardTile::Letter(TileId(1)),
                BoardTile::Letter(TileId(20)),
            ], // arka
        });
        let mut board = Board::new();
        board.put(7, 7, Some(BoardTile::Letter(TileId(20))));
        board.put(7, 8, Some(BoardTile::Letter(TileId(0))));
        board.put(7, 9, Some(BoardTile::Letter(TileId(20))));
        board.put(7, 10, Some(BoardTile::Letter(TileId(2))));
        board.put(5, 8, Some(BoardTile::Letter(TileId(7))));
        board.put(6, 8, Some(BoardTile::Letter(TileId(23))));
        board.put(8, 8, Some(BoardTile::Letter(TileId(20))));
        board.put(9, 8, Some(BoardTile::Letter(TileId(3))));
        board.put(10, 8, Some(BoardTile::Letter(TileId(0))));
        let mut game = Game {
            players: smallvec![Player {
                user_id: UserId(0),
                score: 0,
                rack: ArrayVec::try_from(&[TileId(1), TileId(15), TileId(20),][..]).unwrap()
            }],
            board,
            bag: vec![],
            current_player: 0,
            consecutive_scoreless_moves: 0,
            rng: StdRng::from_seed([44; 32]),
        };
        let move_result = the_move.apply(&mut game, &lexicon);
        assert_eq!(move_result.unwrap().score_added, 7);
    }
}

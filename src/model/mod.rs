/// The model for the game.
pub mod board;
pub mod game;
pub mod lexicon;
pub mod moves;
pub mod parse;
pub mod tile;

#[cfg(test)]
mod tests {
    use super::tile::TILES;

    #[test]
    fn test() {
        // Make sure we don’t inadvertently change the total tile count
        assert_eq!(TILES.iter().map(|t| t.count as usize).sum::<usize>(), 100);
    }
}

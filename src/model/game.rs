//! Items related to the game.

use std::fmt::Display;

use arrayvec::ArrayVec;
use rand::{rngs::StdRng, Rng, SeedableRng};
use serenity::model::prelude::UserId;
use smallvec::SmallVec;

use crate::model::{
    board::Board,
    tile::{TileId, TILES},
};

pub const RACK_SIZE: usize = 7;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum EndReason {
    EmptyRack,
    Passed,
    Left,
}

impl Display for EndReason {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EndReason::EmptyRack => f.write_str("ve miutik kib luut"),
            EndReason::Passed => f.write_str("soan sefik ras rak onkel"),
            EndReason::Left => f.write_str("il soan leevik"),
        }
    }
}

#[derive(Clone, Debug)]
pub struct GameFinish {
    pub penalties: Vec<i32>,
    pub reason: EndReason,
}

/// A player in the game.
#[derive(Clone, Debug)]
pub struct Player {
    /// The ID of the Discord user who is playing.
    pub user_id: UserId,
    /// The player’s current score.
    pub score: i32,
    /// The player’s current rack.
    pub rack: ArrayVec<TileId, RACK_SIZE>,
}

impl Player {
    pub(crate) fn replenish_from_bag(&mut self, bag: &mut Vec<TileId>, rng: &mut StdRng) {
        while !bag.is_empty() && !self.rack.is_full() {
            let i = rng.gen_range(0..bag.len());
            let tile = bag.swap_remove(i);
            self.rack.push(tile);
        }
        self.rack.sort_unstable();
    }
}

/// The state of a game.
#[derive(Clone, Debug)]
pub struct Game {
    /// The players in this game.
    pub players: SmallVec<[Player; 2]>,
    /// The current board.
    pub board: Board,
    /// The tiles remaining in the bag.
    pub bag: Vec<TileId>,
    /// The index of the current player in `players`.
    pub current_player: usize,
    /// The number of moves since the last scoring move.
    pub consecutive_scoreless_moves: u32,
    /// The random number generator used for this game.
    pub(crate) rng: StdRng,
}

impl Game {
    pub fn new_with_default_rng(player_ids: impl Iterator<Item = UserId>) -> Self {
        let rng = StdRng::from_entropy();
        Self::new(player_ids, rng)
    }

    /// Creates a new game with the given players.
    pub fn new(player_ids: impl Iterator<Item = UserId>, mut rng: StdRng) -> Self {
        let mut bag = Self::create_bag();
        let players: SmallVec<[Player; 2]> = player_ids
            .map(|player_id| {
                let mut player = Player {
                    user_id: player_id,
                    score: 0,
                    rack: ArrayVec::new(),
                };
                player.replenish_from_bag(&mut bag, &mut rng);
                player
            })
            .collect();
        let current_player = rng.gen_range(0..players.len());
        Self {
            players,
            board: Board::new(),
            bag,
            current_player,
            consecutive_scoreless_moves: 0,
            rng,
        }
    }

    fn create_bag() -> Vec<TileId> {
        let mut bag = Vec::with_capacity(TILES.iter().map(|t| t.count as usize).sum::<usize>());
        for (i, tile) in TILES.iter().enumerate() {
            for _ in 0..tile.count {
                bag.push(TileId(i as u8));
            }
        }
        bag
    }

    #[inline]
    pub fn current_player(&self) -> &Player {
        &self.players[self.current_player]
    }

    pub fn get_end_reason(&self) -> Option<EndReason> {
        if self.players.iter().any(|player| player.rack.is_empty()) {
            return Some(EndReason::EmptyRack);
        }
        if (self.consecutive_scoreless_moves as usize) >= 3 * self.players.len() {
            return Some(EndReason::Passed);
        }
        None
    }

    pub fn finish(&mut self, reason: EndReason) -> GameFinish {
        let penalties = self
            .players
            .iter_mut()
            .map(|player| {
                let penalty = player
                    .rack
                    .iter()
                    .map(|tile| tile.get_data().value as i32)
                    .sum::<i32>();
                player.score -= penalty;
                penalty
            })
            .collect();
        GameFinish { penalties, reason }
    }

    pub fn remove_player(&mut self, player: UserId) -> Option<GameFinish> {
        let Some(pos) = self.players.iter().position(|x| x.user_id == player) else {
            return None;
        };
        self.players.remove(pos);
        if self.current_player > pos {
            self.current_player -= 1;
        }
        if self.players.len() < 2 {
            return Some(self.finish(EndReason::Left));
        }
        None
    }
}

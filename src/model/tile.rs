//! Items related to game tiles.

use std::fmt::{Display, Write};

/// The data for a tile.
#[derive(Debug, Clone, Copy)]
pub struct Tile {
    /// The letter described by the tile.
    pub letter: char,
    /// The value of this tile.
    pub value: u8,
    /// The number of tiles the game has.
    pub count: u8,
}

impl Tile {
    #[inline]
    pub const fn new(letter: char, value: u8, count: u8) -> Self {
        Tile {
            letter,
            value,
            count,
        }
    }
}

/// Tile frequencies and values using [Nias Avelantis’s letter frequency data](https://w.atwiki.jp/hrain/pages/84.html).
/*
Points | ×1 | ×2  | ×3 | ×4 | ×5 | ×8   | ×10 | ×12 |
     0 |    | ?   |    |    |    |      |     |     |
     1 |    |     | r  | ku | so | tnli | e   | a   |
     2 |    |     | x  | m  |    |      |     |     |
     3 |    | vfd |    |    |    |      |     |     |
     4 | pz |     |    |    |    |      |     |     |
     5 | gb | y   |    |    |    |      |     |     |
     8 | j  |     |    |    |    |      |     |     |
    10 | cw |     |    |    |    |      |     |     |
*/
pub const TILES: [Tile; 26] = [
    Tile::new('t', 1, 8),
    Tile::new('k', 1, 4),
    Tile::new('x', 2, 3),
    Tile::new('s', 1, 5),
    Tile::new('n', 1, 8),
    Tile::new('v', 3, 2),
    Tile::new('f', 3, 2),
    Tile::new('m', 2, 4),
    Tile::new('d', 3, 2),
    Tile::new('g', 5, 1),
    Tile::new('p', 4, 1),
    Tile::new('b', 5, 1),
    Tile::new('h', 4, 1),
    Tile::new('y', 5, 2),
    Tile::new('c', 10, 1),
    Tile::new('r', 1, 3),
    Tile::new('z', 4, 1),
    Tile::new('j', 8, 1),
    Tile::new('w', 10, 1),
    Tile::new('l', 1, 8),
    Tile::new('a', 1, 12),
    Tile::new('i', 1, 8),
    Tile::new('o', 1, 5),
    Tile::new('e', 1, 10),
    Tile::new('u', 1, 4),
    Tile::new('?', 0, 2), // Blank
];

/// A wrapper around the ID of the tile.
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug, PartialOrd, Ord)]
pub struct TileId(pub u8);

impl TileId {
    /// Gets a [`TileId`] by its character.
    pub fn from_char(c: char) -> Option<Self> {
        TILES
            .iter()
            .position(|t| t.letter == c)
            .map(|i| TileId(i as u8))
    }

    #[inline]
    pub fn get_data(self) -> Tile {
        TILES[self.0 as usize]
    }

    pub const BLANK: Self = Self(25);

    #[inline]
    pub fn is_blank(self) -> bool {
        self == Self::BLANK
    }
}

impl Display for TileId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(self.get_data().letter)
    }
}

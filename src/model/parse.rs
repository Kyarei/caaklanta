use arrayvec::ArrayVec;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{anychar, char, space0},
    combinator::{complete, eof, map, map_opt, map_res, verify},
    error::ErrorKind,
    multi::{many0, many1},
    sequence::{delimited, pair, preceded, terminated},
    IResult,
};

use super::{
    board::{self, BoardTile, Orientation, Origin},
    moves::{Move, PlayMove, SwapMove},
    tile::TileId,
};

#[derive(Debug, Clone)]
pub struct ParseError(pub String);

fn parse_tile(input: &str) -> IResult<&str, TileId> {
    map_opt(anychar, TileId::from_char)(input)
}

fn parse_nonblank_tile(input: &str) -> IResult<&str, TileId> {
    verify(parse_tile, |t| !t.is_blank())(input)
}

fn parse_row(input: &str) -> IResult<&str, usize> {
    map_res(nom::character::complete::u8, |n| match n {
        1..=15 => Ok((n - 1) as usize),
        _ => Err(nom::error::Error::new(input, ErrorKind::TooLarge)),
    })(input)
}

fn parse_col(input: &str) -> IResult<&str, usize> {
    map_res(anychar, |c| {
        board::parse_col(c).ok_or_else(|| nom::error::Error::new(input, ErrorKind::TooLarge))
    })(input)
}

fn parse_origin(input: &str) -> IResult<&str, Origin> {
    alt((
        map(pair(parse_row, parse_col), |(row, col)| Origin {
            row,
            col,
            orientation: Orientation::Right,
        }),
        map(pair(parse_col, parse_row), |(col, row)| Origin {
            row,
            col,
            orientation: Orientation::Down,
        }),
    ))(input)
}

fn parse_board_tile(input: &str) -> IResult<&str, BoardTile> {
    alt((
        map(parse_nonblank_tile, BoardTile::Letter),
        map(
            delimited(char('['), parse_nonblank_tile, char(']')),
            BoardTile::Blank,
        ),
    ))(input)
}

fn do_parse_move(input: &str) -> IResult<&str, Move> {
    alt((
        map(tag("sef"), |_| Move::Pass),
        map_res(
            preceded(char('-'), many0(preceded(space0, parse_tile))),
            |letters| {
                Ok::<Move, nom::error::Error<_>>(Move::Swap(SwapMove {
                    letters: ArrayVec::try_from(&letters[..])
                        .map_err(|_| nom::error::Error::new(input, ErrorKind::TooLarge))?,
                }))
            },
        ),
        map(
            pair(parse_origin, preceded(space0, many1(parse_board_tile))),
            |(origin, word)| {
                Move::Play(PlayMove {
                    origin,
                    word: word.into(),
                })
            },
        ),
    ))(input)
}

pub fn parse_move(msg: &str) -> Result<Move, String> {
    let msg = msg.trim();
    match terminated(complete(do_parse_move), eof)(msg) {
        Ok((_, m)) => Ok(m),
        Err(e) => Err(e.to_string()),
    }
}

#[cfg(test)]
mod tests {
    use arrayvec::ArrayVec;
    use smallvec::smallvec;

    use crate::model::{
        board::{BoardTile, Orientation, Origin},
        moves::{Move, PlayMove, SwapMove},
        tile::TileId,
    };

    use super::parse_move;

    #[test]
    fn parse_play_move() {
        let m = parse_move("12v sodn").unwrap();
        assert_eq!(
            m,
            Move::Play(PlayMove {
                origin: Origin {
                    row: 11,
                    col: 5,
                    orientation: Orientation::Right,
                },
                word: smallvec![
                    BoardTile::Letter(TileId(3)),
                    BoardTile::Letter(TileId(22)),
                    BoardTile::Letter(TileId(8)),
                    BoardTile::Letter(TileId(4))
                ]
            })
        );
        let m = parse_move("y3 c[u]ukiite").unwrap();
        assert_eq!(
            m,
            Move::Play(PlayMove {
                origin: Origin {
                    row: 2,
                    col: 13,
                    orientation: Orientation::Down,
                },
                word: smallvec![
                    BoardTile::Letter(TileId(14)),
                    BoardTile::Blank(TileId(24)),
                    BoardTile::Letter(TileId(24)),
                    BoardTile::Letter(TileId(1)),
                    BoardTile::Letter(TileId(21)),
                    BoardTile::Letter(TileId(21)),
                    BoardTile::Letter(TileId(0)),
                    BoardTile::Letter(TileId(23)),
                ]
            })
        );
    }

    #[test]
    fn parse_pass_move() {
        let m = parse_move("sef").unwrap();
        assert_eq!(m, Move::Pass);
    }

    #[test]
    fn parse_swap_move() {
        let m = parse_move("- cw v").unwrap();
        assert_eq!(
            m,
            Move::Swap(SwapMove {
                letters: ArrayVec::try_from(&[TileId(14), TileId(18), TileId(5)] as &[_]).unwrap()
            })
        );
        let m = parse_move("-a?e").unwrap();
        assert_eq!(
            m,
            Move::Swap(SwapMove {
                letters: ArrayVec::try_from(&[TileId(20), TileId(25), TileId(23)] as &[_]).unwrap()
            })
        );
    }

    #[test]
    fn parse_invalid() {
        fn assert_errs(s: &str) {
            if let Ok(o) = parse_move(s) {
                panic!("{:?} successfully parsed to {:?}", s, o)
            }
        }
        assert_errs("0t fite");
        assert_errs("19s onlos");
        assert_errs("d78 xa");
        assert_errs("f4 ta vet");
        assert_errs("d1 i?irius");
        assert_errs("- tkxsnvfm");
    }
}

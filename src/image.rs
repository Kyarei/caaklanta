use poise::serenity_prelude::UserId;
use ril::{
    Border, Font, HorizontalAnchor, Image, LinearGradient, Rectangle, Rgb, TextLayout, TextSegment,
    VerticalAnchor,
};
use serenity::async_trait;

use crate::model::{
    board::{get_board_space, get_col_char, BoardTile, Space, BOARD_SIZE},
    game::{Game, RACK_SIZE},
    tile::{TileId, TILES},
};

const TILE_SIZE: u32 = 32;
const C_MARGIN: u32 = 24;
const MARGIN: u32 = 16;
const TILE_X_OFF: u32 = 5;
const RACK_X: u32 = IMG_HEIGHT + MARGIN;
const RACK_Y: u32 = IMG_HEIGHT - C_MARGIN - TILE_SIZE * 3 / 2;
const IMG_WIDTH: u32 = RACK_X + (RACK_SIZE as u32) * TILE_SIZE + 2 * MARGIN;
const IMG_HEIGHT: u32 = C_MARGIN + MARGIN + (BOARD_SIZE as u32) * TILE_SIZE;
const BLANK_RECT_MARGIN: u32 = 3;
const PLAYER_INFO_HEIGHT: u32 = 32;
const LETTER_SIZE: f32 = 20.0;
const LETTER_SIZE_BLANK: f32 = 16.0;
const VALUE_SIZE: f32 = 10.0;
const PLAYER_INFO_SIZE: f32 = 16.0;
const COORD_SIZE: f32 = 12.0;

#[derive(Clone)]
pub struct DisplayContext {
    gift: Font,
    inje: Font,
    letter_widths_on_letter: Vec<u32>,
    letter_widths_on_blank: Vec<u32>,
    number_widths: Vec<u32>,
    base_image: Image<Rgb>,
}

impl DisplayContext {
    pub fn create() -> ril::Result<Self> {
        let gift = Font::open("data/gift_arnant.ttf", LETTER_SIZE)?;
        let inje = Font::open("data/inje_arnant.ttf", LETTER_SIZE_BLANK)?;
        // Precompute widths for efficiency
        let letter_widths_on_letter = TILES
            .iter()
            .map(|tile| {
                let tl = TextLayout::new().with_position(0, 0).with_segment(
                    &TextSegment::new(&gift, format!("{}", tile.letter), Rgb::black())
                        .with_size(LETTER_SIZE),
                );
                tl.width()
            })
            .collect();
        let letter_widths_on_blank = TILES
            .iter()
            .map(|tile| {
                let tl = TextLayout::new().with_position(0, 0).with_segment(
                    &TextSegment::new(&inje, format!("{}", tile.letter), Rgb::black())
                        .with_size(LETTER_SIZE_BLANK),
                );
                tl.width()
            })
            .collect();
        let max_tile_value = TILES.iter().map(|tile| tile.value).max().unwrap_or(1);
        let number_widths = (1..=max_tile_value)
            .map(|n| {
                let tl = TextLayout::new().with_position(0, 0).with_segment(
                    &TextSegment::new(&gift, format!("{}", n), Rgb::black()).with_size(VALUE_SIZE),
                );
                tl.width()
            })
            .collect();
        // Draw the coordinates
        let mut base_image = Image::new(IMG_WIDTH, IMG_HEIGHT, Rgb::white());
        for row in 0..BOARD_SIZE {
            let text = TextLayout::new()
                .with_position(18, C_MARGIN + TILE_SIZE * (row as u32) + TILE_SIZE / 2)
                .with_horizontal_anchor(HorizontalAnchor::Right)
                .with_vertical_anchor(VerticalAnchor::Center)
                .with_segment(
                    &TextSegment::new(&inje, format!("{}", row + 1), Rgb::black())
                        .with_size(COORD_SIZE),
                );
            base_image.draw(&text);
        }
        for col in 0..BOARD_SIZE {
            let text = TextLayout::new()
                .with_position(C_MARGIN + TILE_SIZE * (col as u32) + TILE_SIZE / 2, 18)
                .with_horizontal_anchor(HorizontalAnchor::Center)
                .with_vertical_anchor(VerticalAnchor::Bottom)
                .with_segment(
                    &TextSegment::new(&inje, format!("{}", get_col_char(col)), Rgb::black())
                        .with_size(COORD_SIZE),
                );
            base_image.draw(&text);
        }
        Ok(DisplayContext {
            gift,
            inje,
            letter_widths_on_letter,
            letter_widths_on_blank,
            number_widths,
            base_image,
        })
    }
}

#[async_trait]
pub trait InfoProvider {
    async fn get_username(&self, user_id: UserId) -> String;
}

pub struct DummyInfoProvider;

#[async_trait]
impl InfoProvider for DummyInfoProvider {
    async fn get_username(&self, user_id: UserId) -> String {
        format!("{user_id}")
    }
}

pub async fn render_game(
    game: &Game,
    ctx: &DisplayContext,
    player_idx: Option<usize>,
    info_provider: &impl InfoProvider,
) -> Image<Rgb> {
    // TODO: most of the stuff doesn’t depend on player_idx; it should be possible to separate the player_idx-dependent stuff
    // TODO: somehow figure out how to highlight newly placed tiles
    let mut image = ctx.base_image.clone();
    // Draw the board
    for row in 0..BOARD_SIZE {
        for col in 0..BOARD_SIZE {
            let x = C_MARGIN + (col as u32) * TILE_SIZE;
            let y = C_MARGIN + (row as u32) * TILE_SIZE;
            let tile = game.board.get(row, col);
            let fill = if matches!(tile, Some(Some(_))) {
                Rgb::new(252, 234, 189)
            } else {
                space_color(get_board_space(row, col))
            };
            let rectangle = Rectangle::at(x, y)
                .with_size(TILE_SIZE, TILE_SIZE)
                .with_border(Border::new(Rgb::black(), 1))
                .with_fill(fill);
            image.draw(&rectangle);
            // Draw contents of tile
            match tile {
                Some(Some(BoardTile::Letter(letter))) => {
                    draw_tile_letter(letter, ctx, x, y, &mut image)
                }
                Some(Some(BoardTile::Blank(letter))) => {
                    draw_blank_marker(x, y, &mut image);
                    let ld = letter.get_data();
                    let letter_text =
                        TextSegment::new(&ctx.inje, format!("{}", ld.letter), Rgb::black())
                            .with_size(LETTER_SIZE_BLANK)
                            .with_position(
                                x + (TILE_SIZE - ctx.letter_widths_on_blank[letter.0 as usize]) / 2,
                                y + TILE_X_OFF,
                            );
                    image.draw(&letter_text);
                }
                _ => (),
            }
        }
    }
    // Draw the rack
    if let Some(player_idx) = player_idx {
        let player = &game.players[player_idx];
        for (i, tile) in player.rack.iter().enumerate() {
            let x = RACK_X + (i as u32) * TILE_SIZE;
            let y = RACK_Y;
            let fill = Rgb::new(252, 234, 189);
            let rectangle = Rectangle::at(x, y)
                .with_size(TILE_SIZE, TILE_SIZE)
                .with_border(Border::new(Rgb::black(), 1))
                .with_fill(fill);
            image.draw(&rectangle);
            draw_tile_letter(*tile, ctx, x, y, &mut image);
        }
    }
    // Draw player info
    for (i, player) in game.players.iter().enumerate() {
        let fill = if i == game.current_player {
            LinearGradient::new()
                .with_angle_degrees(45.0)
                .with_color(Rgb::new(220, 240, 255))
                .with_color(Rgb::new(200, 220, 255))
        } else {
            LinearGradient::new()
                .with_angle_degrees(45.0)
                .with_color(Rgb::new(240, 240, 240))
                .with_color(Rgb::new(200, 200, 200))
        };
        let y = (i as u32) * PLAYER_INFO_HEIGHT;
        let rect = Rectangle::at(IMG_HEIGHT, y)
            .with_size(IMG_WIDTH - IMG_HEIGHT, PLAYER_INFO_HEIGHT)
            .with_fill(fill);
        image.draw(&rect);

        let username = info_provider.get_username(player.user_id).await;
        let username_color = if Some(i) == player_idx {
            Rgb::new(0, 12, 50)
        } else {
            Rgb::black()
        };
        let username_text = TextSegment::new(&ctx.inje, username, username_color)
            .with_position(IMG_HEIGHT + 2, y + 2)
            .with_size(PLAYER_INFO_SIZE);
        image.draw(&username_text);

        let score_text = TextLayout::new()
            .with_horizontal_anchor(HorizontalAnchor::Right)
            .with_position(IMG_WIDTH - 4, y + 14)
            .with_segment(
                &TextSegment::new(&ctx.inje, format!("{}", player.score), Rgb::new(64, 64, 64))
                    .with_size(PLAYER_INFO_SIZE),
            );
        image.draw(&score_text);
    }
    // Draw bag contents
    let bag_text = TextSegment::new(
        &ctx.gift,
        format!("mesl: {} kots", game.bag.len()),
        Rgb::black(),
    )
    .with_position(RACK_X, RACK_Y - 16)
    .with_size(12.0);
    image.draw(&bag_text);

    image
}

fn draw_tile_letter(letter: TileId, ctx: &DisplayContext, x: u32, y: u32, image: &mut Image<Rgb>) {
    if letter.is_blank() {
        draw_blank_marker(x, y, image);
        return;
    }
    let ld = letter.get_data();
    let letter_text = TextSegment::new(&ctx.gift, format!("{}", ld.letter), Rgb::black())
        .with_size(LETTER_SIZE)
        .with_position(
            x + (TILE_SIZE - ctx.letter_widths_on_letter[letter.0 as usize]) / 2,
            y + TILE_X_OFF,
        );
    image.draw(&letter_text);
    let number_text = TextSegment::new(&ctx.gift, format!("{}", ld.value), Rgb::black())
        .with_size(VALUE_SIZE)
        .with_position(
            x + (TILE_SIZE - ctx.number_widths[(ld.value - 1) as usize]) - 4,
            y + 22,
        );
    image.draw(&number_text);
}

fn draw_blank_marker(x: u32, y: u32, image: &mut Image<Rgb>) {
    let rect = Rectangle::<Rgb>::at(x + BLANK_RECT_MARGIN, y + BLANK_RECT_MARGIN)
        .with_size(
            TILE_SIZE - 2 * BLANK_RECT_MARGIN,
            TILE_SIZE - 2 * BLANK_RECT_MARGIN,
        )
        .with_border(Border::new(Rgb::black(), 1));
    image.draw(&rect);
}

fn space_color(space: Space) -> Rgb {
    match space {
        Space::XX => Rgb::new(180, 204, 196),
        Space::L2 => Rgb::new(156, 252, 249),
        Space::L3 => Rgb::new(45, 131, 252),
        Space::W2 => Rgb::new(252, 164, 225),
        Space::W3 => Rgb::new(244, 17, 77),
    }
}

#[cfg(test)]
mod tests {
    use poise::serenity_prelude::UserId;
    use rand::{rngs::StdRng, SeedableRng};

    use crate::model::{board::BoardTile, game::Game, tile::TileId};

    use super::{render_game, DisplayContext, DummyInfoProvider};

    #[tokio::test]
    async fn test_image_gen() {
        let ctx = DisplayContext::create().unwrap();
        let info_provider = DummyInfoProvider;
        let mut game = Game::new(
            [UserId(1), UserId(2)].into_iter(),
            StdRng::from_seed([44; 32]),
        );
        game.board.put(4, 5, Some(BoardTile::Letter(TileId(14))));
        game.board.put(8, 14, Some(BoardTile::Blank(TileId(15))));
        game.players[1].score = 44;
        let image = render_game(&game, &ctx, Some(0), &info_provider).await;
        image
            .save(ril::ImageFormat::Png, "target/test.png")
            .unwrap();
    }
}

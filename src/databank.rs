use std::{
    fmt::Display,
    ops::{Deref, DerefMut},
};

use caaklanta::model::game::{Game, GameFinish};
use dashmap::{mapref::entry::Entry, DashMap};
use poise::serenity_prelude::{ChannelId, Message};
use serenity::{
    http::CacheHttp,
    model::id::{MessageId, UserId},
};
use smallvec::{smallvec, SmallVec};

use crate::util::user_name;

#[derive(Clone, Debug)]
pub enum MatchmakingError {
    AlreadyInGame,
    NoSuchGame,
    AlreadyStarted,
    Pending,
    NotInGame,
}

impl Display for MatchmakingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MatchmakingError::AlreadyInGame => f.write_str("ti xa liva moal"),
            MatchmakingError::NoSuchGame => f.write_str("yuu liva oken tu fala xa"),
            MatchmakingError::AlreadyStarted => f.write_str("tu liva es kit moal"),
            MatchmakingError::Pending => f.write_str("tu liva es kit moal"),
            MatchmakingError::NotInGame => f.write_str("ti xa yuu liva"),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct GameId(pub ChannelId, pub MessageId);

impl GameId {
    pub fn from_message(message: &Message) -> Self {
        GameId(message.channel_id, message.id)
    }
}

/// A game that has not yet started.
#[derive(Clone, Debug, Default)]
pub struct PendingGame {
    requester: UserId,
    players: SmallVec<[UserId; 2]>,
    max_players: usize,
}

impl PendingGame {
    pub fn new(requester: UserId, capacity: usize) -> Self {
        Self {
            requester,
            players: smallvec![requester],
            max_players: capacity,
        }
    }

    pub fn add_player(&mut self, player: UserId) {
        self.players.push(player);
    }

    pub fn is_ready(&self) -> bool {
        self.players.len() >= self.max_players
    }

    pub async fn to_message(&self, cache_http: &impl CacheHttp) -> String {
        let mut s = format!(
            "**{}** kit lax liva e caak!\n{}/{} soan: ",
            user_name(self.requester, cache_http).await,
            self.players.len(),
            self.max_players,
        );
        let mut first = true;
        for player in &self.players {
            if !first {
                s += ", ";
            }
            first = false;
            s += &user_name(*player, cache_http).await;
        }
        s
    }
}

#[derive(Debug)]
pub struct GameRef<'a>(dashmap::mapref::one::Ref<'a, GameId, Game>);

impl<'a> GameRef<'a> {
    pub fn id(&self) -> GameId {
        *self.0.key()
    }
}

impl<'a> Deref for GameRef<'a> {
    type Target = Game;

    fn deref(&self) -> &Self::Target {
        self.0.value()
    }
}

#[derive(Debug)]
pub struct GameRefMut<'a>(dashmap::mapref::one::RefMut<'a, GameId, Game>);

impl<'a> GameRefMut<'a> {
    pub fn id(&self) -> GameId {
        *self.0.key()
    }
}

impl<'a> Deref for GameRefMut<'a> {
    type Target = Game;

    fn deref(&self) -> &Self::Target {
        self.0.value()
    }
}

impl<'a> DerefMut for GameRefMut<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0.value_mut()
    }
}

#[derive(Debug)]
pub struct PendingGameRef<'a>(dashmap::mapref::one::Ref<'a, GameId, PendingGame>);

impl<'a> Deref for PendingGameRef<'a> {
    type Target = PendingGame;

    fn deref(&self) -> &Self::Target {
        self.0.value()
    }
}

#[derive(Clone, Debug, Default)]
pub struct Databank {
    // TODO: maybe use SQLite or something and get persistence
    pending_games_by_id: DashMap<GameId, PendingGame>,
    active_games_by_id: DashMap<GameId, Game>,
    games_by_user: DashMap<UserId, GameId>,
}

impl Databank {
    pub fn game_by_id(&self, id: GameId) -> Option<GameRef<'_>> {
        self.active_games_by_id.get(&id).map(GameRef)
    }

    pub fn game_by_id_mut(&self, id: GameId) -> Option<GameRefMut<'_>> {
        self.active_games_by_id.get_mut(&id).map(GameRefMut)
    }

    pub fn game_id_by_user(&self, user: UserId) -> Option<GameId> {
        Some(*self.games_by_user.get(&user)?.value())
    }

    pub fn game_by_user(&self, user: UserId) -> Option<GameRef<'_>> {
        self.game_by_id(self.game_id_by_user(user)?)
    }

    pub fn game_by_user_mut(&self, user: UserId) -> Option<GameRefMut<'_>> {
        self.game_by_id_mut(self.game_id_by_user(user)?)
    }

    pub fn open_new_game(
        &self,
        requester: UserId,
        game_id: GameId,
        capacity: usize,
    ) -> Result<Result<GameRefMut, PendingGameRef>, MatchmakingError> {
        let Entry::Vacant(game_by_user_entry) = self.games_by_user.entry(requester) else {
            return Err(MatchmakingError::AlreadyInGame);
        };
        game_by_user_entry.insert(game_id);
        let Entry::Vacant(game_entry) = self.pending_games_by_id.entry(game_id) else {
            return Err(MatchmakingError::Pending);
        };
        let mut game_entry = game_entry.insert_entry(PendingGame::new(requester, capacity));
        let pending = game_entry.get_mut();
        if pending.is_ready() {
            let game = self
                .active_games_by_id
                .entry(game_id)
                .insert(Game::new_with_default_rng(pending.players.iter().copied()));
            game_entry.remove();
            Ok(Ok(GameRefMut(game)))
        } else {
            Ok(Err(PendingGameRef(game_entry.into_ref().downgrade())))
        }
    }

    pub fn join_game(
        &self,
        user_id: UserId,
        game_id: GameId,
    ) -> Result<Result<GameRefMut, PendingGameRef>, MatchmakingError> {
        let Entry::Vacant(game_by_user_entry) = self.games_by_user.entry(user_id) else {
            return Err(MatchmakingError::AlreadyInGame);
        };
        let Entry::Occupied(mut game_entry) = self.pending_games_by_id.entry(game_id) else {
            if self.active_games_by_id.contains_key(&game_id) {
                return Err(MatchmakingError::AlreadyStarted);
            }
            return Err(MatchmakingError::NoSuchGame);
        };
        let pending = game_entry.get_mut();
        game_by_user_entry.insert(game_id);
        pending.add_player(user_id);
        if pending.is_ready() {
            let game = self
                .active_games_by_id
                .entry(game_id)
                .insert(Game::new_with_default_rng(pending.players.iter().copied()));
            game_entry.remove();
            Ok(Ok(GameRefMut(game)))
        } else {
            Ok(Err(PendingGameRef(game_entry.into_ref().downgrade())))
        }
    }

    pub fn end_game(&self, game: GameId) -> bool {
        let mut removed = false;
        if let Some((_, pending_game)) = self.pending_games_by_id.remove(&game) {
            for player in &pending_game.players {
                self.games_by_user.remove(player);
            }
            removed = true;
        }
        if let Some((_, active_game)) = self.active_games_by_id.remove(&game) {
            for player in &active_game.players {
                self.games_by_user.remove(&player.user_id);
            }
            removed = true;
        }
        removed
    }

    pub fn leave_game(&self, user_id: UserId) -> Result<(GameId, LeaveResult), MatchmakingError> {
        let Entry::Occupied(user_entry) = self.games_by_user.entry(user_id) else {
            return Err(MatchmakingError::NotInGame);
        };

        let game_id = *user_entry.get();

        if let Entry::Occupied(mut pending) = self.pending_games_by_id.entry(game_id) {
            let pending_game = pending.get_mut();
            pending_game.players.retain(|x| *x != user_id);
            user_entry.remove();

            if pending_game.players.len() < 2 {
                for player in &pending_game.players {
                    self.games_by_user.remove(player);
                }
                return Ok((game_id, LeaveResult::EndedPending(pending.remove())));
            }
            return Ok((
                game_id,
                LeaveResult::LeftPending(PendingGameRef(pending.into_ref().downgrade())),
            ));
        }

        if let Entry::Occupied(mut active) = self.active_games_by_id.entry(game_id) {
            let active_game = active.get_mut();
            if let Some(finish) = active_game.remove_player(user_id) {
                user_entry.remove();
                for player in &active_game.players {
                    self.games_by_user.remove(&player.user_id);
                }
                return Ok((game_id, LeaveResult::EndedActive(active.remove(), finish)));
            } else {
                user_entry.remove();
                return Ok((
                    game_id,
                    LeaveResult::LeftActive(GameRef(active.into_ref().downgrade())),
                ));
            }
        }

        Err(MatchmakingError::NotInGame)
    }
}

#[derive(Debug)]
pub enum LeaveResult<'a> {
    LeftActive(GameRef<'a>),
    EndedActive(Game, GameFinish),
    LeftPending(PendingGameRef<'a>),
    EndedPending(PendingGame),
}

mod command;
mod databank;
mod util;

#[tokio::main]
async fn main() {
    let _ = dotenvy::dotenv();

    command::start().await;
}

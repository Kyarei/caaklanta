use caaklanta::{
    image::{render_game, DisplayContext},
    model::{
        game::{Game, GameFinish},
        lexicon::Lexicon,
        moves::{Move, MoveError, MoveFeedback},
        parse::parse_move,
    },
};
use poise::{
    serenity_prelude::{Message, Reaction, ReactionType, UserId},
    BoxFuture, Event, FrameworkContext, FrameworkOptions, PrefixFrameworkOptions,
};
use ril::ImageFormat;

use crate::{
    databank::{Databank, GameId, LeaveResult, MatchmakingError},
    util::{user_name, DiscordInfoProvider},
};
use std::{env, fmt::Display, fmt::Write, fs::File, io::BufReader, mem};

use serenity::prelude::GatewayIntents;

const PREFIX: &str = "cl!";
const JOIN_EMOJI: &str = "🥕";

pub async fn start() {
    let lexicon =
        Lexicon::read(&mut BufReader::new(File::open("data/veteda.txt").unwrap())).unwrap();
    let data = CaaklantaData::new(lexicon).unwrap();

    // Login with a bot token from the environment
    let token = env::var("DISCORD_TOKEN").expect("token");
    let intents = GatewayIntents::non_privileged() | GatewayIntents::DIRECT_MESSAGES;
    let framework = poise::Framework::builder()
        .options(FrameworkOptions {
            commands: vec![kit(), leev()],
            prefix_options: PrefixFrameworkOptions {
                prefix: Some(PREFIX.into()),
                ..Default::default()
            },
            event_handler: handle_misc_event,
            ..Default::default()
        })
        .token(token)
        .intents(intents)
        .setup(|ctx, _ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                Ok(data)
            })
        });

    framework.run().await.unwrap();
}

type Context<'a> = poise::Context<'a, CaaklantaData, CaaklantaError>;

#[derive(Debug)]
pub enum CaaklantaError {
    Matchmaking(MatchmakingError),
    Move(MoveError),
    NotYourTurn,
    What,
    PlayerCount,
    Serenity(serenity::Error),
    Ril(ril::Error),
}

impl From<MatchmakingError> for CaaklantaError {
    fn from(value: MatchmakingError) -> Self {
        CaaklantaError::Matchmaking(value)
    }
}

impl From<MoveError> for CaaklantaError {
    fn from(value: MoveError) -> Self {
        CaaklantaError::Move(value)
    }
}

impl From<serenity::Error> for CaaklantaError {
    fn from(value: serenity::Error) -> Self {
        CaaklantaError::Serenity(value)
    }
}

impl From<ril::Error> for CaaklantaError {
    fn from(value: ril::Error) -> Self {
        CaaklantaError::Ril(value)
    }
}

impl Display for CaaklantaError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CaaklantaError::Matchmaking(e) => Display::fmt(e, f),
            CaaklantaError::Move(e) => Display::fmt(e, f),
            CaaklantaError::NotYourTurn => f.write_str("tu de sast tiil"),
            CaaklantaError::What => f.write_str("to???"),
            CaaklantaError::PlayerCount => f.write_str("soanalx dim (et fal 1_8)"),
            CaaklantaError::Serenity(e) => write!(f, "caaklanta fesik: {e}"),
            CaaklantaError::Ril(e) => write!(f, "caaklanta fesik: {e}"),
        }
    }
}

pub struct CaaklantaData {
    lexicon: Lexicon,
    databank: Databank,
    display_ctx: DisplayContext,
}

impl CaaklantaData {
    pub fn new(lexicon: Lexicon) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self {
            lexicon,
            databank: Databank::default(),
            display_ctx: DisplayContext::create()?,
        })
    }
}

static HELP: &str = r#"tu et sast tiil.
re ev vet a siz:
* ev vet hix: **<kiis><niim> <vet>**. ova **5k miiko**.
* ev vet has: **<niim><kiis> <vet>**. ova **p2 oma**.
* ev kots miut kon arvij (`[]`) sanlen hacm.
**sef**: sef sast tiil.
**-<hacm>**: fut kots a mesl. ova el fut sen *b*, *c*, o kots miut kon **-?bc**."#;

async fn send_game_image(
    game: &Game,
    display_ctx: &DisplayContext,
    s_ctx: &serenity::prelude::Context,
    ended: bool,
) -> Result<(), CaaklantaError> {
    let info_provider = DiscordInfoProvider(s_ctx);
    for (i, player) in game.players.iter().enumerate() {
        let image = render_game(game, display_ctx, Some(i), &info_provider).await;
        let mut image_bytes = Vec::<u8>::new();
        image.encode(ImageFormat::Png, &mut image_bytes)?;
        let player_id = player.user_id;
        let dm_channel = player_id
            .to_user(s_ctx)
            .await?
            .create_dm_channel(s_ctx)
            .await?;
        let content = if !ended && i == game.current_player {
            HELP
        } else {
            ""
        };
        dm_channel
            .send_message(&s_ctx.http, |m| {
                m.add_file((&image_bytes[..], "liva.png")).content(content)
            })
            .await?;
    }
    Ok(())
}

async fn notify_start(
    game: &Game,
    display_ctx: &DisplayContext,
    s_ctx: &serenity::prelude::Context,
) -> Result<(), CaaklantaError> {
    for player in &game.players {
        let player_id = player.user_id;
        let dm_channel = player_id
            .to_user(s_ctx)
            .await?
            .create_dm_channel(s_ctx)
            .await?;
        dm_channel
            .send_message(&s_ctx.http, |m| m.content("liva tiil ik kit!"))
            .await?;
    }
    send_game_image(game, display_ctx, s_ctx, false).await?;
    Ok(())
}

async fn notify_move(
    game: &Game,
    display_ctx: &DisplayContext,
    s_ctx: &serenity::prelude::Context,
    mover_id: UserId,
    m: Option<(&Move, &MoveFeedback)>,
    game_finish: Option<GameFinish>,
) -> Result<(), CaaklantaError> {
    // TODO:
    // * don’t show swap contents to everyone
    let mut content = match m {
        Some((m, feedback)) => format!(
            "**{}** soletik **{}** (+{})",
            user_name(mover_id, &s_ctx).await,
            m,
            feedback.score_added,
        ),
        None => format!(
            "**{}** leevik liva. :frowning:",
            user_name(mover_id, &s_ctx).await,
        ),
    };
    let ended = game_finish.is_some();
    if let Some(game_finish) = game_finish {
        let _ = write!(&mut content, ". liva ik took mil {}.\n", game_finish.reason);
        for (player, penalty) in game.players.iter().zip(game_finish.penalties.iter()) {
            let _ = write!(
                &mut content,
                "{}: -{} ⇒ **{}**\n",
                user_name(player.user_id, &s_ctx).await,
                *penalty,
                player.score
            );
        }
    }
    for player in &game.players {
        let player_id = player.user_id;
        let dm_channel = player_id
            .to_user(s_ctx)
            .await?
            .create_dm_channel(s_ctx)
            .await?;
        dm_channel
            .send_message(&s_ctx.http, |m| m.content(&content))
            .await?;
    }
    send_game_image(game, display_ctx, s_ctx, ended).await?;
    Ok(())
}

fn handle_misc_event<'a>(
    ctx: &'a serenity::prelude::Context,
    event: &'a Event,
    _fctx: FrameworkContext<'a, CaaklantaData, CaaklantaError>,
    data: &'a CaaklantaData,
) -> BoxFuture<'a, Result<(), CaaklantaError>> {
    Box::pin(async move {
        match event {
            Event::Message { new_message } => {
                if let Err(e) = handle_message(new_message, ctx, data).await {
                    new_message.reply(ctx, &e).await?;
                    Err(e)?;
                }
            }
            Event::ReactionAdd { add_reaction } => {
                if let Err(e) = handle_reaction(add_reaction, ctx, data).await {
                    add_reaction
                        .channel_id
                        .send_message(ctx, |m| {
                            m.content(format!(
                                "<@{}>, {}",
                                add_reaction.user_id.unwrap_or_default(),
                                e
                            ))
                        })
                        .await?;
                    Err(e)?;
                }
            }
            _ => (),
        }
        Ok(())
    })
}

async fn handle_message<'a>(
    new_message: &Message,
    ctx: &'a serenity::prelude::Context,
    data: &'a CaaklantaData,
) -> Result<(), CaaklantaError> {
    let player_id = new_message.author.id;

    let Ok(channel) = new_message.channel(ctx).await else {
        return Ok(());
    };
    // Only listen to move messages in DMs
    if channel.guild().is_some() {
        return Ok(());
    }
    // Only listen if this player is in a game
    let Some(mut game) = data.databank.game_by_user_mut(player_id) else {
        return Ok(());
    };

    // Make sure it’s this player’s turn
    if game.current_player().user_id != player_id {
        return Err(CaaklantaError::NotYourTurn);
    }
    let mut m = parse_move(&new_message.content).map_err(|_| CaaklantaError::What)?;
    let feedback = m.apply(&mut game, &data.lexicon)?;
    m.canonicalize(&game.board);

    let end_reason = game.get_end_reason();
    let finish = end_reason.map(|r| game.finish(r));

    let r = notify_move(
        &game,
        &data.display_ctx,
        ctx,
        player_id,
        Some((&m, &feedback)),
        finish,
    )
    .await;

    if end_reason.is_some() {
        let id = game.id();
        mem::drop(game);
        data.databank.end_game(id);
    }

    r?;

    Ok(())
}
async fn handle_reaction<'a>(
    add_reaction: &Reaction,
    ctx: &'a serenity::prelude::Context,
    data: &'a CaaklantaData,
) -> Result<(), CaaklantaError> {
    if add_reaction.emoji.unicode_eq(JOIN_EMOJI) {
        let game_id = GameId(add_reaction.channel_id, add_reaction.message_id);
        let user = add_reaction.user(ctx).await?;
        if user.bot {
            return Ok(());
        }
        let game = data.databank.join_game(user.id, game_id)?;
        let mut origin_message = add_reaction.message(&ctx.http).await?;
        match game {
            Ok(active) => {
                let _ = origin_message.delete(ctx).await;
                notify_start(&active, &data.display_ctx, ctx).await?;
            }
            Err(pending) => {
                let content = pending.to_message(&ctx).await;
                let _ = origin_message.edit(ctx, |m| m.content(content)).await;
            }
        }
    }

    Ok(())
}

/// kit liva sam.
#[poise::command(slash_command, prefix_command)]
async fn kit(ctx: Context<'_>, soanalx: Option<usize>) -> Result<(), CaaklantaError> {
    let soanalx = soanalx.unwrap_or(2);
    if !(1..=8).contains(&soanalx) {
        return Err(CaaklantaError::PlayerCount);
    }
    let requester = ctx.author();
    let requester_id = requester.id;
    let message = ctx
        .say(format!(
            "**{}** kit lax liva e caak!\nfakt tu kon :carrot:!",
            requester.name
        ))
        .await?
        .into_message()
        .await?;
    message
        .react(&ctx, ReactionType::Unicode(JOIN_EMOJI.into()))
        .await?;
    let game_id = GameId::from_message(&message);
    let databank = &ctx.data().databank;
    match databank.open_new_game(requester_id, game_id, soanalx) {
        Err(e) => {
            let _ = message.delete(&ctx).await;
            Err(e)?;
        }
        Ok(Ok(active)) => {
            let _ = message.delete(&ctx).await;
            notify_start(&active, &ctx.data().display_ctx, ctx.serenity_context()).await?;
        }
        Ok(Err(_)) => (),
    }
    Ok(())
}

/// leev liva tiil.
#[poise::command(slash_command, prefix_command)]

async fn leev(ctx: Context<'_>) -> Result<(), CaaklantaError> {
    let data = ctx.data();
    let user_id = ctx.author().id;
    let (left_game_id, leave_game) = data.databank.leave_game(user_id)?;
    ctx.send(|reply| reply.content("ti leevik liva.")).await?;
    match leave_game {
        LeaveResult::LeftActive(game) => {
            notify_move(
                &game,
                &data.display_ctx,
                ctx.serenity_context(),
                user_id,
                None,
                None,
            )
            .await?;
            Ok(())
        }
        LeaveResult::EndedActive(game, finish) => {
            notify_move(
                &game,
                &data.display_ctx,
                ctx.serenity_context(),
                user_id,
                None,
                Some(finish),
            )
            .await?;
            Ok(())
        }
        LeaveResult::LeftPending(game) => {
            let mut message = left_game_id.0.message(ctx, left_game_id.1).await?;
            let content = game.to_message(&ctx).await;
            message.edit(ctx, |m| m.content(content)).await?;
            Ok(())
        }
        LeaveResult::EndedPending(_game) => {
            let message = left_game_id.0.message(ctx, left_game_id.1).await?;
            message.delete(ctx).await?;
            Ok(())
        }
    }
}

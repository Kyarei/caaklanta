use caaklanta::image::InfoProvider;
use poise::serenity_prelude::UserId;
use serenity::{async_trait, http::CacheHttp};

pub async fn user_name(user_id: UserId, cache_http: impl CacheHttp) -> String {
    user_id
        .to_user(cache_http)
        .await
        .map_or("???".into(), |user| user.name)
}

pub struct DiscordInfoProvider<'a>(pub &'a serenity::prelude::Context);

#[async_trait]
impl<'a> InfoProvider for DiscordInfoProvider<'a> {
    async fn get_username(&self, user_id: UserId) -> String {
        user_name(user_id, self.0).await
    }
}
